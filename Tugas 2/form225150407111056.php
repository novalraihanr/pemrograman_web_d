<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <form action="Form225150407111056.php" method="get">
        <h1>FORM</h1>
        <label for="">NIM</label>
        <input type="text" name="nim" id="nim">
        
        <br>

        <label for="">Nama</label>
        <input type="text" name="nama" id="nama">
        
        <br>

        <label for="">Umur</label>
        <input type="number" name="umur" id="umur">

        <br>

        <label for="">Program Studi: </label> <br>
        <input type="radio" name="program_studi" id="sistem_informasi" value="Sistem Informasi">
        <label for="sistem_informasi">Sistem Informasi</label> <br>
        <input type="radio" name="program_studi" id="teknologi_informasi" value="Teknologi Informasi"> 
        <label for="teknologi_informasi">Teknologi Informasi</label> <br>
        <input type="radio" name="program_studi" id="teknik_informatika" value="Teknik Informatika">
        <label for="teknik_informatika">Teknik Informatika</label> <br>
        <input type="radio" name="program_studi" id="pendidikan_ti" value="Pendidikan Teknologi Informasi">
        <label for="pendidikan_ti">Pendidikan Teknologi Informasi</label> <br>

        <br>

        <label for="hobi">Pilih Hobi</label> <br>
        <select name="hobi" id="hobi">
            <option value="tidur">Tidur</option>
            <option value="makan">Makan</option>
            <option value="nongkorong">Nongkrong</option>
            <option value="">Main</option>
        </select>

        <br>
        <br>

        <input type="submit" value="Submit">
    </form>

    <br>
    <?php 
    if(isset($_GET['nama']) && isset($_GET['nim']) && isset($_GET['umur']) && isset($_GET['program_studi']) && isset($_GET['hobi'])){
        $nama = $_GET['nama'];
        $nim = $_GET['nim'];
        $umur = $_GET['umur'];
        $programstd = $_GET['program_studi'];
        $hobi = $_GET['hobi'];

        // echo "nama = ", $nama, "<br>";
        // echo "nim = ", $nim, "<br>";
        // echo "umur = ", $umur, "<br>";
        // echo "program studi = ", $programstd, "<br>";
        // echo "hobi = ", $hobi, "<br>";

        echo "<table border=1>";
        echo "<tr><td>Nama</td>";
        echo "<td>",$nama,"</td></tr>";
        echo "<tr><td>NIM</td>";
        echo "<td>",$nim,"</td></tr>";
        echo "<tr><td>Umur</td>";
        echo "<td>",$umur,"</td></tr>";
        echo "<tr><td>Program Studi</td>";
        echo "<td>",$programstd,"</td></tr>";
        echo "<tr><td>Hobi</td>";
        echo "<td>",$hobi,"</td></tr>";
        echo "</table>";
    }else{
        echo "<h2>Mohon Lengkapi Form</h2>";
    }
    ?>
</body>
</html>